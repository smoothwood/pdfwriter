﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFWriter.Helpers
{
    public static class CommonHelpers
    {
        public static JObject GetJObject(this Stream stream)
        {
            StreamReader reader = new StreamReader(stream, Encoding.UTF8);
            string jsonString = reader.ReadToEnd();

            JObject jsonObj = (JObject)JsonConvert.DeserializeObject(jsonString);
            return jsonObj;
        }
    }
}
