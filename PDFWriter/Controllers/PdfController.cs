﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp.text.pdf;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PDFWriter.Helpers;

namespace PDFWriter.Controllers
{
    /*
     +------------------------------------------------------------------------------------------------+
     | Created by ycm                                                                                 |      
     | Generate Pdf by using iTextSharp.LGPLv2.Core library                                           |
     | Post below json to generate new pdf                                                            |
     |                                                                                                |
     |              {                                                                                 |           
     |                  "templateFile":"template.pdf",                                                |
     |                  "newFile":"newpdf.pdf",                                                       |
     |                  "data":[                                                                      |
     |                          {"key":"field name","value":"field value","type":"text"},             |
     |                          {"key":"field name","value":"field value","type":"text"},             |
     |                          {"key":"field name","value":"field value","type":"text"},             |
     |                          {"key":"field name","value":"image url","type":"image"},              |
     |                  ]                                                                             |
     |              }                                                                                 |
     |                                                                                                |
     | Put pdf template under "template" folder, new generated pdf will be put under "new" folder     |
     | Pield  name needs to be pre-defined in pdf, type for checkbox is "text" too                    |
     | Two possible values for checkbox "Yes" and "Off"                                               |
     | Image url could be the absolute path on disk or url                                            |
     | send POST to https://localhost:44367/api/Pdf/PopulateFields                                    |
     | Use URL https://localhost:44367/api/Pdf/GetFields?templateFile=filename.pdf to get all fields  |
     +------------------------------------------------------------------------------------------------+
    */
    [Route("api/[controller]")]
    [ApiController]
    public class PdfController : ControllerBase
    {
        IHostingEnvironment _env;
        public PdfController(IHostingEnvironment env)
        {
            _env = env;
        }

        [HttpPost]
        [Route("PopulateFields")]
        public void PopulateFields()
        {
            string templateFileDirectory = Path.Combine(_env.ContentRootPath, "template");
            string templateFile = "";

            string newFileDirectory = Path.Combine(_env.ContentRootPath, "new");
            string newFile = "";

            JArray data = null;

            JObject jsonObj = Request.Body.GetJObject();
            JEnumerable<JToken> children = jsonObj.Children();

            //get json content
            foreach(JToken child in children)
            {
                if(((Newtonsoft.Json.Linq.JProperty)child).Name.ToLower() == "templatefile")
                {
                    templateFile = ((Newtonsoft.Json.Linq.JProperty)child).Value.ToString();
                }
                else if(((Newtonsoft.Json.Linq.JProperty)child).Name.ToLower() == "newfile")
                {
                    newFile = ((Newtonsoft.Json.Linq.JProperty)child).Value.ToString();
                }
                else if(((Newtonsoft.Json.Linq.JProperty)child).Name.ToLower() == "data")
                {
                    //all fields values contained in data node of json
                    data =(JArray)((Newtonsoft.Json.Linq.JProperty)child).Value;
                }
            }
            //proces fields
            PopulatePdfFields(templateFile, newFile, data);
        }


        private void PopulatePdfFields(string templateFile, string newFile, JArray data)
        {
            string templateFilePath = Path.Combine(_env.ContentRootPath, "template", templateFile);
            string newFilePath = Path.Combine(_env.ContentRootPath, "new", newFile);

            PdfReader pdfReader = new PdfReader(templateFilePath);
            FileStream newFileStream = new FileStream(newFilePath, FileMode.Create);
            PdfStamper pdfStamper = new PdfStamper(pdfReader, newFileStream);
            AcroFields pdfFormFields = pdfStamper.AcroFields;

            for(int i = 0; i < data.Count(); i++)
            {
                JObject field = (JObject)data[i];
                string key = field.Value<string>("key");
                string value = field.Value<string>("value");
                string type = field.Value<string>("type");

                //if text field
                if(type == "text")
                {
                    pdfFormFields.SetField(key, value);
                }
                else if(type == "image")
                {
                    PushbuttonField pbf = pdfFormFields.GetNewPushbuttonFromField(key);
                    pbf.Layout = PushbuttonField.LAYOUT_ICON_ONLY;
                    pbf.ProportionalIcon = true;
                    pbf.Image = iTextSharp.text.Image.GetInstance(value);
                    pdfFormFields.ReplacePushbuttonField(key, pbf.Field);
                }
            }

            pdfStamper.FormFlattening = false;
            // close the pdf and stream
            pdfStamper.Close();
            newFileStream.Close();
        }




        [HttpGet]
        [Route("GetFields")]
        public ActionResult<string> GetFields([FromQuery(Name = "templateFile")]string templateFile)
        {
            string templateFilePath = Path.Combine(_env.ContentRootPath, "template", templateFile);
            Stream pdfStream = new FileStream(templateFilePath, FileMode.Open);
            ICollection fieldNames = GetPdfFields(pdfStream);
            string fields = "";
            foreach (var fieldName in fieldNames)
            {
                fields += fieldName.ToString() + "\n";
            }
            pdfStream.Close();
            return fields;
        }

        private ICollection GetPdfFields(Stream pdfStream)
        {
            PdfReader reader = null;
            try
            {
                PdfReader pdfReader = new PdfReader(pdfStream);
                AcroFields acroFields = pdfReader.AcroFields;
                return acroFields.Fields.Keys;
            }
            finally
            {
                reader?.Close();
            }
        }
    }
}